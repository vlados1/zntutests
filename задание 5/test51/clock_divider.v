`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:36:50 03/01/2015 
// Design Name: 
// Module Name:    clock_divider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clock_divider(
    input clk,
    output [0:7] half_second
    );

reg [27:0] counter;

    // count up on each positive clock
    always @(posedge clk)
            counter <= counter + 1;

    // one second signal would be emitted when 
    // 26th bit of the counter would become "1"
    assign half_second[0] = counter[23];
	 assign half_second[1] = counter[24];
	 assign half_second[2] = counter[23];
	 assign half_second[3] = counter[22];
	 assign half_second[4] = counter[23];
	 assign half_second[5] = counter[21];
	 assign half_second[6] = counter[22];
	 assign half_second[7] = counter[21];
endmodule

module clock_divider2(
    input clk2,
    output quarter_second
    );

reg [23:0] counter;

    // count up on each positive clock
    always @(posedge clk2)
            counter <= counter + 1;

    // one second signal would be emitted when 
    // 26th bit of the counter would become "1"
    assign quarter_second = counter[23];
endmodule
